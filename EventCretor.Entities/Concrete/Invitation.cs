﻿using EventCreator.Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreator.Entities.Concrete
{
    [Table("invitation")]
    public class Invitation:IEntity
    {
        [Key]
        public int Id { get; set; }
        public int EventId { get; set; }
        public int UserId { get; set; }
        public int Response { get; set; }
    }

    public enum InvitationResponseType
    {
        YES = 1,
        NO = 0,
        MAYBE = 2,
        NORESPONSE =-1
    }
}
