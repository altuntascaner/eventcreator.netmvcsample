﻿using EventCreator.Core.Entities;

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreator.Entities.Concrete
{
    [Table("event")]
    public class Event : IEntity
    {
        [Key]
        public virtual int Id { get; set; }
        public virtual string Title { get; set; }
        public virtual string Description { get; set; }
        public virtual string StartDate { get; set; }
        public virtual string EndDate { get; set; }
        public virtual int CreatedBy { get; set; }
        public virtual string CreatedDate { get; set; }
        public virtual int? ModifiedBy { get; set; }
        public virtual string ModifiedDate { get; set; }
    }
}
