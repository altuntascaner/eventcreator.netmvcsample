﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventCreator.Entities.Concrete;

namespace EventCreator.Entities.ComplexTypes
{
    public class InvitedGuest
    {
        public Event Event { get; set; }
        public User Guest { get; set; }
        public Invitation Invitation { get; set; }
    }
}
