﻿using EventCreator.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreator.Entities.ComplexTypes
{
    public class EventUserJoin
    {
        public Event eventItem { get; set; }
        public User CreatedBy { get; set; }
    }
}
