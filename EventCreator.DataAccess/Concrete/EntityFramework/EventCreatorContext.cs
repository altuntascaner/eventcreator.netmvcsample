﻿
using EventCreator.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreator.DataAccess.Concrete.EntityFramework
{
    public class EventCreatorContext:DbContext
    {
        public EventCreatorContext()
            : base("EventCreatorContext")
        {           
        }

        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Event> Events { get; set; }
        public virtual DbSet<Invitation> Invitations { get; set; }

    }
}
