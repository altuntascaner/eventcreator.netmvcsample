﻿using EventCreator.Core.DataAccess.EntityFramework;
using EventCreator.DataAccess.Abstract;
using EventCreator.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreator.DataAccess.Concrete.EntityFramework
{
    public class EfUserRepository : EfEntityRepositoryBase<User, EventCreatorContext>, IUserDal
    {
        public List<User> GetAddableUserListForEvent(string query, int eventId, int userId)
        {
            var res = new List<User>();

            using (var context = new EventCreatorContext())
            {
                res = context.Users
                             .Where(x => x.Id != userId
                                         && ((x.FirstName+x.LastName).ToLower().Contains(query.Trim().ToLower())
                                         || (x.FirstName +" "+ x.LastName).ToLower().Contains(query.Trim().ToLower()))
                                         && !context.Invitations
                                                   .Any(y => y.EventId == eventId && y.UserId == x.Id)
                                    )
                             .ToList();
            }

            return res;
        }
    }
}
