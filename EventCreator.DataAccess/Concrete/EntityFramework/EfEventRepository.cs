﻿using EventCreator.Core.DataAccess.EntityFramework;
using EventCreator.DataAccess.Abstract;
using EventCreator.Entities.ComplexTypes;
using EventCreator.Entities.Concrete;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreator.DataAccess.Concrete.EntityFramework
{
    public class EfEventRepository : EfEntityRepositoryBase<Event, EventCreatorContext>, IEventDal
    {
        public List<EventUserJoin> GetAllEventsRelatedToUser(int userId)
        {
            var res = new List<EventUserJoin>();

            using (var context = new EventCreatorContext())
            {
                 res = (from e in context.Events
                              join u in context.Users on e.CreatedBy equals u.Id 
                       where e.CreatedBy == userId 
                      select new EventUserJoin() {
                          eventItem = e,
                          CreatedBy = u
                      }).ToList();

                res.AddRange((from e in context.Events
                                     join u in context.Users on e.CreatedBy equals u.Id   
                                     join i in context.Invitations on e.Id equals i.EventId
                                     where i.UserId == userId
                                     select new EventUserJoin()
                                     {
                                         eventItem = e,
                                         CreatedBy = u
                                     }).ToList());
            }

            return res;
        }

        public List<InvitedGuest> GetGuestsOfEvent(int eventId)
        {
            var res = new List<InvitedGuest>();

            using (var context = new EventCreatorContext())
            {
                res = (from e in context.Events
                       join i in context.Invitations on e.Id equals i.EventId
                       join u in context.Users on i.UserId equals u.Id
                       where e.Id == eventId
                       select new InvitedGuest() {
                           Guest = u,
                           Event = e,
                           Invitation = i
                       }).ToList();
            }

            return res;

        }
    }
}
