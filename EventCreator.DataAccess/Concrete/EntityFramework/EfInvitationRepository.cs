﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventCreator.Entities.Concrete;
using EventCreator.Core.DataAccess.EntityFramework;
using EventCreator.DataAccess.Abstract;


namespace EventCreator.DataAccess.Concrete.EntityFramework
{
    public class EfInvitationRepository:EfEntityRepositoryBase<Invitation,EventCreatorContext>,IInvitationDal
    {
    }
}
