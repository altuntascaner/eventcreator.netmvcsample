﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using EventCreator.Core.DataAccess;
using EventCreator.Entities.Concrete;

namespace EventCreator.DataAccess.Abstract
{
    public interface IUserDal : IEntityRepository<User>
    {
        List<User> GetAddableUserListForEvent(string query, int eventId, int userId);
    }
}
