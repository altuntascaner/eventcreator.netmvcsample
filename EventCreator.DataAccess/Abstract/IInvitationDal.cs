﻿using EventCreator.Core.DataAccess;
using EventCreator.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreator.DataAccess.Abstract
{
    public interface IInvitationDal:IEntityRepository<Invitation>
    {
    }
}
