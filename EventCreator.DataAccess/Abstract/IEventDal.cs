﻿using EventCreator.Core.DataAccess;
using EventCreator.Entities.Concrete;
using EventCreator.Entities.ComplexTypes;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreator.DataAccess.Abstract
{
    public interface IEventDal:IEntityRepository<Event>
    {
        List<EventUserJoin> GetAllEventsRelatedToUser(int userId);
        List<InvitedGuest> GetGuestsOfEvent(int eventId);
    }
}
