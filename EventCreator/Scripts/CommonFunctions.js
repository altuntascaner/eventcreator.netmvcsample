﻿function IsEmptyOrNull(input) {
    return input == null || input == '' || input === undefined
}

function GetDateTimeNow() {
    var today = new Date();

    var d = today.getDate();
    d = (d + '').length < 2 ? '0' + d : d;

    var m = (today.getMonth() + 1);

    m = (m + '').length < 2 ? '0' + m : m;

    var date = d + '-' + m + '-' + today.getFullYear();

    var h = today.getHours();
    h = (h + '').length < 2 ? '0' + h : h;

    var mi = today.getHours();
    mi = (mi + '').length < 2 ? '0' + mi : mi;

    var time = h + ":" + mi;

    return date + " " + time;
}

function messagePopUp(message,type,posFrom,posAlign) {
    $.notify({
        // options
        icon: 'glyphicon '+(type=='success'?'glyphicon-ok-circle':'glyphicon-warning-sign'),
        message: message
    }, {
            type: type,
            placement: {
                from: posFrom,
                align: posAlign
            },
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
        });
}