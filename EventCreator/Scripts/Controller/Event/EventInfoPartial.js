﻿$(document).ready(function () {

    if (!IsEmptyOrNull($('#StartDateHidden').val())) {
        var sDateFormatted = formatRawDate($('#StartDateHidden').val());
        $('#dtStartDate').data("DateTimePicker").date(sDateFormatted);
        $('#StartDate').val(sDateFormatted);
    }

    if (!IsEmptyOrNull($('#EndDateHidden').val())) {
        var eDateFormatted = formatRawDate($('#EndDateHidden').val());
        $('#dtEndDate').data("DateTimePicker").date(eDateFormatted);
        $('#EndDate').val(eDateFormatted);
    }
});

function formatRawDate(input) {
    var y = input.substr(0, 4);
    var m = input.substr(4, 2);
    var d = input.substr(6, 2);
    var h = input.substr(8, 2);
    var mi = input.substr(10, 2);

    return d + "-" + m + "-" + y + " " + h + ":" + mi;
}

$('.chkFilterGuests').change(function () {
    var isChecked = $(this).prop('checked');
    var responseType = $(this).attr('responseType');

    var elems = $('.guestsListItem[responseType="' + responseType + '"]');

    if (isChecked) {
        elems.show();
    }
    else {
        elems.hide();
    }

    setGuestCountText();
});

function setGuestCountText() {
    $('#divGuestCount').html($('.guestsListItem:visible').length + " guest(s)");

    var totalCount = $('.guestsListItem').length;

    $('#divTotalGuestText').html(totalCount + ' guest' + (totalCount > 1 ? "s are" : " is")+' invited.');    
}

$('#btnSaveEventData').click(function () {

    var title = $('#Title').val();
    var desc = $('#Description').val();
    var startDate = $('#StartDate').val();

    if (IsEmptyOrNull(title) || IsEmptyOrNull(desc) || IsEmptyOrNull(startDate)) {
        messagePopUp('Please fill require * fields',"warning","top","left");
    }
    else {
        $.ajax({
            type: "POST",
            async: false,
            //contentType: "application/json; charset=utf-8",
            url: "/Event/SaveEventInfo",
            data: $('#formEventInfo').serialize(),
            dataType: "text",
            success: function (data) {

                if (data != 'nok') {
                    var op = data.split('-')[0];
                    var id = data.split('-')[1];

                    if (op == 'added') {
                        window.location = '/Event/EventInfo/' + id;
                    }
                    else {
                        messagePopUp('The event is updated successfully', "success", "top", "left");
                    }
                }
            }
        });
    }
});

$('#btnDeleteEvent').click(function () {

    $('#deleteEventConfirmModal').modal('show');
});

$('#btnDeleteEventConfirm').click(function () {
    $.ajax({
        type: "POST",
        async: false,
        //contentType: "application/json; charset=utf-8",
        url: "/Event/DeleteEvent",
        data: $('#formEventInfo').serialize(),
        dataType: "text",
        success: function (data) {
            window.location = '/MainPage'
        }
    });
});

$(document).on('click','.removeUserFromGuests',function () {
    var invitationId = $(this).closest('.guestsListItem').attr('invitationId');

    $('#deleteGuestConfirmModal').attr('invitationId', invitationId);
    $('#deleteGuestConfirmModal').modal('show');

    
});

$('#btnDeleteGuestFromList').click(function () {
    var invitationId = $('#deleteGuestConfirmModal').attr('invitationId');

    $.ajax({
        type: "POST",
        async: false,
        contentType: "application/json; charset=utf-8",
        url: "/Invitation/RemoveGuestFromEvent",
        data: JSON.stringify({ invitationId: invitationId }),
        dataType: "text",
        success: function (data) {

            $('.guestsListItem[invitationid="' + invitationId + '"]').remove();

            setGuestCountText();

            $('#deleteGuestConfirmModal').modal('hide');

            messagePopUp("The guest is deleted from the list","success","top","right")
        }
    });
})

$("#txtUserList").autocomplete({
    source: function (request, response) {
        $.ajax({
            type:'POST',
            url: "/Event/GetAddableUserListForEvent",
            data: { query: request.term, eventId: $('#formEventInfo #Id').val(), userId: $('#txtCurrentUserId').val() },
            success: function (data) {                
                response(data);
            },
            error: function () {
                response([]);
            }
        });
    }
    ,search: function () {
        $('#txtSelectedGuestId').val('');
    }
    ,select: function (e,obj) {
        $('#txtSelectedGuestId').val(obj.item.id);
    }
});

$('#btnAddUserToGuestList').click(function () {
    var userId = $('#txtSelectedGuestId').val();

    if (!IsEmptyOrNull(userId)) {
        var eventId = $('#formEventInfo #Id').val();

        $('#addGuestConfirmModal').attr('eventId', eventId);
        $('#addGuestConfirmModal').attr('userId', userId);

        $('#addGuestConfirmModal').modal('show');
    }
    else {
        messagePopUp('Please select a guest from the list',"warning","bottom","right");
    }
});

$('#btnAddGuest').click(function () {
    var eventId = $('#addGuestConfirmModal').attr('eventId');
    var userId = $('#addGuestConfirmModal').attr('userId');

    $.ajax({
        type: 'POST',
        url: "/Invitation/AddGuestToEvent",
        data: { eventId: eventId, userId: userId },
        success: function (res) {
            if (res != "nok") {

                var html = '<div class="guestsListItem"'
                    + 'responseType="-1"'
                    + 'invitationId="' + res + '" >'
                    + $("#txtUserList").val()
                    + '<a class="removeUserFromGuests">'
                    + '<i class="glyphicon glyphicon-remove"></i>'
                    + '</a>'
                    + '</div>';

                $('#divGuestList').prepend(html);
                $('#divGuestList').scrollTop(0);

                setGuestCountText();

                $('#txtSelectedGuestId').val('');
                $('#txtUserList').val('');

                $('#addGuestConfirmModal').modal('hide');

                messagePopUp("The guest is added to the list.", "success", "bottom", "right");
            }
            else {
                messagePopUp("Operation failed.", "danger", "bottom", "right");
            }
        }
    });
});

$('.btnInvitationResponse').click(function () {
    var invitationId = $(this).attr('invitationId');
    var response = $(this).attr('response');

    $.ajax({
        type: 'POST',
        url: "/Invitation/ResponseToInvitation",
        data: { invitationId: invitationId, response: response },
        success: function (res) {
            if (res == "ok") {
                messagePopUp("Your response is saved.", "success", "bottom", "right");
            }
        }
    });
});

$('#dtStartDate').datetimepicker({
    format: 'DD-MM-YYYY HH:mm'
    , minDate: moment()
    //, update: function (e) {
    //    $('#EndDate').data("DatetimePicker").minDate(e.viewDate);
    //}
}).on('dp.change', function (e) {
    $('#dtEndDate').data("DateTimePicker").minDate(e.date);

    if (!IsEmptyOrNull($('#EndDate').val())) {
        $('#dtEndDate').data("DateTimePicker").date(e.date);
    }
});

$('#dtEndDate').datetimepicker({
    format: 'DD-MM-YYYY HH:mm'
    , minDate: moment()
    //, update: function (e) {
    //    $('#StartDate').data("DatetimePicker").maxDate(e.viewDate);
    //}
});