﻿
$(document).ready(function () {
    $('form').submit(function (e) {
        var firstName = $('#FirstName').val();
        var lastName = $('#LastName').val();
        var email = $('#EMail').val();
        var pass = $('#Password').val();
        var passRepeat = $('#RepeatPassword').val();
        var firstName = $('#FirstName').val();

        if (IsEmptyOrNull(firstName) || IsEmptyOrNull(lastName) || IsEmptyOrNull(email) || IsEmptyOrNull(pass) || IsEmptyOrNull(passRepeat)) {
            e.preventDefault();
            messagePopUp('Please fill all required (*) fields',"warning","top","center");
        }
        else if (!$('#EMail').is('[readonly]') && CheckIfEmailTaken(email) == 'true') {
            e.preventDefault();
            $('#alertEmailTaken').show();
        }
        else if (passRepeat != pass) {
            e.preventDefault();
            messagePopUp('Please correctly repeat the password', "warning", "top", "center");
        }
        
    });

    $('#UserImage').change(function () {
        var tmppath = URL.createObjectURL($(this)[0].files[0]);
        $('#imgUser').attr('src', tmppath);
    });

});

function CheckIfEmailTaken(inputEmail) {
    var res = 'false';

    $.ajax({
        type: "POST",
        async: false,
        url: "/User/CheckIfEmailExists",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({ email: inputEmail }),
        dataType: "text",
        success: function (data) {
            res = data;
        }
    });

    return res;
}