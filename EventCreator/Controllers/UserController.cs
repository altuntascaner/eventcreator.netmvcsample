﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EventCreator.Business.Abstract;
using EventCreator.Entities.Concrete;
using System.IO;

namespace EventCreator.Controllers
{
    public class UserController : Controller
    {
        IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SaveUser(User inputUser,HttpPostedFileBase UserImage)
        {
            inputUser.EMail = inputUser.EMail.ToLower().Trim();

            if (UserImage != null)
            {
                inputUser.ImageVersion = DateTime.Now.ToString("yyyyMMddHHmmss");
            }

            if (inputUser.Id == 0)
            {
                inputUser = _userService.Add(inputUser);
            }
            else
            {
                inputUser = _userService.Update(inputUser);
            }

            if (UserImage != null)
            {
                string fileName = Path.Combine(Server.MapPath("~/Content/Images/User"), inputUser.Id + "_"+ inputUser.ImageVersion + ".jpeg");

                UserImage.SaveAs(fileName);
            }

            Session["user"] = inputUser;
            
            return RedirectToAction("Index", "MainPage");
        }

        public ActionResult UserProfile(string id)
        {
            var user = _userService.GetById(int.Parse(id));

            return View("_UserInfoPartial",user);
        }

        public ActionResult CheckIfEmailExists(string email)
        {
            var existingUser = _userService.GetList(x => x.EMail.ToLower() == email.Trim().ToLower());

            if (existingUser.Count > 0)
            {
                return Content("true");
            }

            return Content("false");
        }
    }
}