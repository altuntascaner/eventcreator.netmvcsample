﻿using EventCreator.Business.Abstract;
using EventCreator.Entities.Concrete;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EventCreator.Views.Login
{  
    public class LoginController : Controller
    {
        IUserService _userService;

        public LoginController(IUserService userService)
        {
            _userService = userService;
        }

        public ActionResult Index()
        {
            return View("Login");
        }

        public ActionResult CheckLogin(string email, string password)
        {
            try
            {
                var user = _userService.CheckLogin(email, password);

                if (user != null)
                {
                    Session["user"] = user;
                    ViewBag.CurrentUserName = user.FirstName + " " + user.LastName;
                    return RedirectToAction("Index", "MainPage");
                }
            }
            catch
            { }

            ViewBag.LoginError = true;

            return View("Login");
        }

        public ActionResult Register()
        {
            var user = new User();

            return View("Register",user);
        }

        public ActionResult LogOut()
        {
            Session["user"] = null;

            return Index();
        }
    }
}