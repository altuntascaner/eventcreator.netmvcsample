﻿using EventCreator.Business.Abstract;
using EventCreator.Entities.Concrete;
using EventCreator.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EventCreator.Controllers
{
    [IsLogin]
    public class InvitationController : Controller
    {
        IInvitationService _invitationService;

        public InvitationController(IInvitationService invitationService)
        {
            _invitationService = invitationService;
        }
        // GET: Invitation
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult RemoveGuestFromEvent(string invitationId)
        {
            Invitation item = _invitationService.GetById(int.Parse(invitationId));

            _invitationService.Delete(item);

            return Content("ok");
        }

        public ActionResult AddGuestToEvent(string userId, string eventId)
        {
            Invitation item = new Invitation();
            item.EventId = int.Parse(eventId);
            item.UserId = int.Parse(userId);
            item.Response = (int)(InvitationResponseType.NORESPONSE);

            item = _invitationService.Add(item);

            if (item.Id > 0)
            {
                return Content(item.Id.ToString());
            }
            else
            {
                return Content("nok");
            }
        }

        public ActionResult ResponseToInvitation(string invitationId, string response)
        {
            var invitation = _invitationService.GetById(int.Parse(invitationId));

            invitation.Response = int.Parse(response);

            _invitationService.Update(invitation);

            return Content("ok");
        }
    }
}