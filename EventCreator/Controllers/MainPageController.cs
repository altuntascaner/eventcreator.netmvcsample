﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EventCreator.Business.Abstract;
using EventCreator.Entities.Concrete;
using EventCreator.Models;
using  EventCreator.Helpers;

namespace EventCreator.Controllers
{
    [IsLogin]
    public class MainPageController : Controller
    {
        IEventService _eventService;

        public MainPageController(IEventService eventService)
        {
            _eventService = eventService;
        }
        // GET: MainPage

        
        public ActionResult Index()
        {
            var user = Session["user"] as User;

            var model = new MainPageModel()
            {
                User = user,
                Events = _eventService.GetAllEventsRelatedToUser(user.Id)
            };

            return View("_MainPage",model);
        }
    }
}