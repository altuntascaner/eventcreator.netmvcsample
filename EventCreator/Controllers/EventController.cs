﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EventCreator.Business.Abstract;
using EventCreator.Entities.Concrete;
using EventCreator.Models;

namespace EventCreator.Controllers
{
    public class EventController : Controller
    {
        IEventService _eventService;
        IUserService _userService;
        
        public EventController(IEventService eventService, IUserService userService)
        {
            _eventService = eventService;
            _userService = userService;
        }

        // GET: Event
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult EventInfo(string id)
        {            
            EventInfoModel model = null;

            if (!string.IsNullOrEmpty(id))
            {
                model = new EventInfoModel()
                {
                    Event = _eventService.GetById(int.Parse(id)),
                    Guests = _eventService.GetGuestsOfEvent(int.Parse(id))
                };
            }
            else
            {
                model = model = new EventInfoModel()
                {
                    Event = new Event(),
                    Guests = new List<Entities.ComplexTypes.InvitedGuest>()
                };
            }

            return View("_EventInfoPartial", model);
        }

        public ActionResult SaveEventInfo(Event inputEvent)
        {
            var datetime = DateTime.Now.ToString("yyyyMMddHHmmss");

            string startDate = DateTime.ParseExact(inputEvent.StartDate, "dd-MM-yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyyMMddHHmm") + "00";

            inputEvent.StartDate = startDate;

            if (inputEvent.EndDate != null)
            {
                inputEvent.EndDate = DateTime.ParseExact(inputEvent.EndDate, "dd-MM-yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture).ToString("yyyyMMddHHmm") + "00";
            }

            if (inputEvent.Id == 0)
            {
                inputEvent.CreatedBy = ((User)Session["user"]).Id;
                inputEvent.CreatedDate = datetime;

                inputEvent = _eventService.Add(inputEvent);

                return Content("added-"+inputEvent.Id);
            }
            else
            {
                inputEvent.ModifiedBy = ((User)Session["user"]).Id;
                inputEvent.ModifiedDate = datetime;

                inputEvent = _eventService.Update(inputEvent);

                return Content("updated-" + inputEvent.Id);
            }
        }

        public ActionResult DeleteEvent(Event inputEvent)
        {
            _eventService.Delete(inputEvent);

            return Content("ok");
        }

        public ActionResult GetAddableUserListForEvent(string query, string eventId,string userId)
        {
            List<User> possibleUsers = _userService.GetAddableUserListForEvent(query,int.Parse(eventId), int.Parse(userId));

            return Json(possibleUsers.Select(x => new { label = x.FirstName + " " + x.LastName, id = x.Id }).ToList());
        }
    }
}