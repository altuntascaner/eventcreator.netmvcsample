﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EventCreator.Entities.ComplexTypes;
using EventCreator.Entities.Concrete;

namespace EventCreator.Models
{
    public class MainPageModel
    {
        public User User { get; set; }
        public List<EventUserJoin> Events { get; set; }
    }
}