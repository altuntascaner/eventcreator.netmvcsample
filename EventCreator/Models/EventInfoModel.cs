﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EventCreator.Entities.Concrete;
using EventCreator.Entities.ComplexTypes;


namespace EventCreator.Models
{
    public class EventInfoModel
    {
        public Event Event { get; set; }
        public List<InvitedGuest> Guests { get; set; }
    }
}