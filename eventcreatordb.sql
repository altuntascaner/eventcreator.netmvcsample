-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Anamakine: 127.0.0.1
-- Üretim Zamanı: 29 May 2019, 20:38:07
-- Sunucu sürümü: 10.1.37-MariaDB
-- PHP Sürümü: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Veritabanı: `eventcreatordb`
--
CREATE DATABASE IF NOT EXISTS `eventcreatordb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `eventcreatordb`;

-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `event`
--

CREATE TABLE `event` (
  `Id` int(11) NOT NULL,
  `Title` varchar(1024) NOT NULL,
  `Description` varchar(4000) NOT NULL,
  `StartDate` varchar(14) NOT NULL,
  `EndDate` varchar(14) DEFAULT NULL,
  `CreatedBy` int(11) NOT NULL,
  `CreatedDate` varchar(14) NOT NULL,
  `ModifiedBy` int(11) DEFAULT NULL,
  `ModifiedDate` varchar(14) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Eklemeden önce tabloyu kes `event`
--

TRUNCATE TABLE `event`;
-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `invitation`
--

CREATE TABLE `invitation` (
  `Id` int(11) NOT NULL,
  `EventId` int(11) NOT NULL,
  `UserId` int(11) NOT NULL,
  `Response` int(11) NOT NULL DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Eklemeden önce tabloyu kes `invitation`
--

TRUNCATE TABLE `invitation`;
-- --------------------------------------------------------

--
-- Tablo için tablo yapısı `user`
--

CREATE TABLE `user` (
  `Id` int(11) NOT NULL,
  `FirstName` varchar(256) NOT NULL,
  `LastName` varchar(256) NOT NULL,
  `EMail` varchar(1024) NOT NULL,
  `Password` varchar(1024) NOT NULL,
  `ImageVersion` varchar(14) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Eklemeden önce tabloyu kes `user`
--

TRUNCATE TABLE `user`;
--
-- Tablo döküm verisi `user`
--

INSERT INTO `user` (`Id`, `FirstName`, `LastName`, `EMail`, `Password`, `ImageVersion`) VALUES
(1, 'Caner', 'Altuntas', 'altuntas.ca@gmail.com', '202CB962AC59075B964B07152D234B70', NULL),
(13, 'Alan', 'Smith', 'alan@gmail.com', '202CB962AC59075B964B07152D234B70', NULL),
(14, 'Hans', 'Berger', 'hans@gmail.com', '202CB962AC59075B964B07152D234B70', NULL);

--
-- Dökümü yapılmış tablolar için indeksler
--

--
-- Tablo için indeksler `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`Id`);

--
-- Tablo için indeksler `invitation`
--
ALTER TABLE `invitation`
  ADD PRIMARY KEY (`Id`);

--
-- Tablo için indeksler `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Id`);

--
-- Dökümü yapılmış tablolar için AUTO_INCREMENT değeri
--

--
-- Tablo için AUTO_INCREMENT değeri `event`
--
ALTER TABLE `event`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- Tablo için AUTO_INCREMENT değeri `invitation`
--
ALTER TABLE `invitation`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- Tablo için AUTO_INCREMENT değeri `user`
--
ALTER TABLE `user`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
