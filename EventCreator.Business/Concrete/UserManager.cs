﻿using EventCreator.Business.Abstract;
using EventCreator.DataAccess.Abstract;
using EventCreator.Entities.Concrete;
using EventCreator.Core.Utilities.Common;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace EventCreator.Business.Concrete
{
    public class UserManager:IUserService
    {
        IUserDal _userDal;

        public UserManager(IUserDal userDal)
        {
            _userDal = userDal;
        }

        public User Add(User newUser)
        {
            newUser.Password = Hash.GetHashedOf(newUser.Password);

            return _userDal.Add(newUser);
        }

        public User CheckLogin(string email, string password)
        {
            string hashedPassword = Hash.GetHashedOf(password);

            return _userDal.Get(x => x.EMail == email && x.Password == hashedPassword);
        }

        public List<User> GetAddableUserListForEvent(string query,int eventId, int userId)
        {
            return _userDal.GetAddableUserListForEvent(query,eventId, userId);
        }

        public User GetById(int id)
        {
            return _userDal.Get(x => x.Id == id);
        }

        public User Update(User user)
        {
            user.Password = Hash.GetHashedOf(user.Password);

            return _userDal.Update(user);
        }

        public List<User> GetList(Expression<Func<User, bool>> filter)
        {
            return _userDal.GetList(filter);
        }
    }
}
