﻿using EventCreator.Business.Abstract;
using EventCreator.DataAccess.Abstract;
using EventCreator.Entities.Concrete;
using EventCreator.Core.Utilities.Common;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using EventCreator.Entities.ComplexTypes;

namespace EventCreator.Business.Concrete
{
    public class EventManager:IEventService
    {
        IEventDal _eventDal;

        public EventManager(IEventDal eventDal)
        {
            _eventDal = eventDal;
        }

        public Event Add(Event newEvent)
        {
            return _eventDal.Add(newEvent);
        }

        public void Delete(Event eventInput)
        {
            _eventDal.Delete(eventInput);
        }

        public List<EventUserJoin> GetAllEventsRelatedToUser(int userId)
        {
            List<EventUserJoin> eventList = _eventDal.GetAllEventsRelatedToUser(userId);

            return eventList;
        }

        public List<InvitedGuest> GetGuestsOfEvent(int eventId)
        {
            List<InvitedGuest> list = _eventDal.GetGuestsOfEvent(eventId);

            return list;
        }

        public List<Event> GetList(Expression<Func<Event, bool>> filter = null)
        {
            return _eventDal.GetList(filter);
        }

        public Event Update(Event eventInput)
        {
            return _eventDal.Update(eventInput);
        }

        Event IEventService.GetById(int id)
        {
            return _eventDal.Get(x => x.Id == id);
        }
    }
}
