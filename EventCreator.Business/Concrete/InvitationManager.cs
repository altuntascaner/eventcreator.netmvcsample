﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EventCreator.Business.Abstract;
using EventCreator.Entities.Concrete;
using EventCreator.DataAccess.Abstract;

namespace EventCreator.Business.Concrete
{
    public class InvitationManager : IInvitationService
    {
        IInvitationDal _invitationDal;

        public InvitationManager(IInvitationDal invitationDal)
        {
            _invitationDal = invitationDal;
        }

        public Invitation Add(Invitation item)
        {
            return _invitationDal.Add(item);
        }

        public void Delete(Invitation item)
        {
            _invitationDal.Delete(item);
        }

        public Invitation GetById(int id)
        {
            return _invitationDal.Get(x => x.Id == id);
        }

        public Invitation Update(Invitation item)
        {
            return _invitationDal.Update(item);
        }
    }
}
