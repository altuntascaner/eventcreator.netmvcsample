﻿using EventCreator.Business.Abstract;
using EventCreator.Business.Concrete;
using EventCreator.DataAccess.Abstract;
using EventCreator.DataAccess.Concrete.EntityFramework;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreator.Business.DependencyResolvers.Ninject
{
    public class BusinessModule:NinjectModule
    {
        public override void Load()
        {
            Bind<IInvitationService>().To<InvitationManager>().InSingletonScope();
            Bind<IInvitationDal>().To<EfInvitationRepository>().InSingletonScope();

            Bind<IEventService>().To<EventManager>().InSingletonScope();
            Bind<IEventDal>().To<EfEventRepository>().InSingletonScope();

            Bind<IUserService>().To<UserManager>().InSingletonScope();
            Bind<IUserDal>().To<EfUserRepository>().InSingletonScope();
        }
    }
}
