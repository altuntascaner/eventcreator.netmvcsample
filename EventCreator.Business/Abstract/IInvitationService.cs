﻿using EventCreator.Core.DataAccess;
using EventCreator.Entities.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EventCreator.Business.Abstract
{
    public interface IInvitationService
    {
        void Delete(Invitation item);
        Invitation GetById(int id);
        Invitation Add(Invitation item);
        Invitation Update(Invitation item);
    }
}
