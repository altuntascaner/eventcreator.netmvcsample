﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using EventCreator.Entities.Concrete;

namespace EventCreator.Business.Abstract
{
    public interface IUserService
    {
        User GetById(int id);

        User CheckLogin(string email, string password);

        User Add(User newUser);

        User Update(User user);

        List<User> GetAddableUserListForEvent(string query,int eventId, int userId);

        List<User> GetList(Expression<Func<User,bool>> filter);
    }
}
