﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using EventCreator.Entities.Concrete;
using EventCreator.Entities.ComplexTypes;

namespace EventCreator.Business.Abstract
{
    public interface IEventService
    {
        Event GetById(int id);

        Event Add(Event newEvent);

        Event Update(Event eventInput);

        void Delete(Event eventInput);


        List<Event> GetList(Expression<Func<Event,bool>> filter = null);

        List<EventUserJoin> GetAllEventsRelatedToUser(int userId);

        List<InvitedGuest> GetGuestsOfEvent(int eventId);
    }
}
