﻿
using EventCreator.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;

namespace EventCreator.Core.DataAccess.EntityFramework
{
    public class EfEntityRepositoryBase<TEntity, Tcontext> : IEntityRepository<TEntity>
        where TEntity : class, IEntity, new()
        where Tcontext:DbContext,new()
    {
        public TEntity Add(TEntity entity)
        {
            using (var context = new Tcontext())
            {
                var addedEntity = context.Entry<TEntity>(entity);
                addedEntity.State = EntityState.Added;

                context.SaveChanges();

                return entity;
            }
        }

        public void Delete(TEntity entity)
        {
            using (var context = new Tcontext())
            {
                var deletedEntity = context.Entry<TEntity>(entity);
                deletedEntity.State = EntityState.Deleted;

                context.SaveChanges();
            }
        }

        public TEntity Get(Expression<Func<TEntity, bool>> filter)
        {
            using (var context = new Tcontext())
            {
                List<TEntity> l = context.Set<TEntity>().ToList();

                return context.Set<TEntity>().SingleOrDefault(filter);
            }
        }

        public List<TEntity> GetList(Expression<Func<TEntity, bool>> filter = null)
        {
            using (var context = new Tcontext())
            {
                return filter == null ? 
                            context.Set<TEntity>().ToList()
                            :context.Set<TEntity>().Where(filter).ToList();
            }
        }

        public TEntity Update(TEntity entity)
        {
            using (var context = new Tcontext())
            {
                var updatedEntity = context.Entry<TEntity>(entity);
                updatedEntity.State = EntityState.Modified;

                context.SaveChanges();

                return entity;
            }
        }
    }
}
